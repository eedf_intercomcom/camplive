from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Register your models here.
from .models import *

class CampAdmin(admin.ModelAdmin):
    list_display = ('titre', 'debut', 'fin')
    ordering = ('debut', )
    readonly_fields = ('created_time', 'updated_time')
    
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.created_by = request.user
        elif change:
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)

class GestionAdmin(admin.ModelAdmin):
    list_display = ('camp', 'user', 'droit')
    ordering = ('-camp__debut', )
    list_filter = ['camp', 'user']

class GestionAjoutAdmin(admin.ModelAdmin):
    list_display = ('created_time', 'camp')
    ordering = ('-created_time', )
    list_filter = ['camp']

class MessageAdmin(admin.ModelAdmin):
    list_display = ("camp", "updated_time")
    ordering = ('-updated_time', )
    list_filter = ['camp']

class PhotoAdmin(admin.ModelAdmin):
    list_display = ("camp", "updated_time")
    ordering = ('-updated_time', )
    list_filter = ['camp']

class AbonnementNewsletterAdmin(admin.ModelAdmin):
    list_display = ("date", "camp", "mail", "statut")
    ordering = ("-date", )
    list_filter = ['camp']

admin.site.register(User, UserAdmin)
admin.site.register(Camp, CampAdmin)
admin.site.register(Gestion, GestionAdmin)
admin.site.register(GestionAjout, GestionAjoutAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(AbonnementNewsletter, AbonnementNewsletterAdmin)