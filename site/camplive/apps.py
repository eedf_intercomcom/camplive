from django.apps import AppConfig


class CampliveConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "camplive"
