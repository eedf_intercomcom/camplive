from django import forms
from .models import *
from django.core.exceptions import ValidationError

class DateInput(forms.DateInput):
    input_type = 'date'

class CampForm(forms.ModelForm):
    class Meta:
        model = Camp
        fields = ['titre', 'debut', 'fin', 'intro', 'autoinscription']
    
    def clean_fin(self):
        debut = self.cleaned_data['debut']
        fin = self.cleaned_data['fin']
        if fin < debut:
            raise ValidationError("La date de fin doit être postérieure à la date de début.")
        return fin

class GestionAjoutForm(forms.ModelForm):
    class Meta:
        model = GestionAjout
        fields = ['mail', 'droit']

class GestionModifForm(forms.ModelForm):
    class Meta:
        model = Gestion
        fields = ['droit']

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['texte']
        widgets = {'texte': forms.Textarea(attrs={'placeholder': "Écrire un message"})}
    
    def clean_texte(self):
        message = self.cleaned_data['texte'].strip()
        if len(message) == 0: raise ValidationError("Le message ne peut pas être vide.")
        return message

class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True

class MultipleFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = [single_file_clean(data, initial)]
        return result

class PhotoForm(forms.Form):
    image = MultipleFileField()

class AbonnementNewsletterForm(forms.ModelForm):
    class Meta:
        model = AbonnementNewsletter
        fields = ['mail']
        widgets = {'mail': forms.EmailInput(attrs={'placeholder': "Saisir une adresse électronique"})}