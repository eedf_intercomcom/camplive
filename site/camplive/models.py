from django.db import models
from django.urls import reverse
from django.contrib.auth.models import AbstractUser
from django.template.defaultfilters import slugify
from django.conf import settings


# Create your models here.
class User(AbstractUser):
    pass

    def __str__(self):
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        else:
            return self.username

class Camp(models.Model):
    titre = models.CharField(max_length = 50, verbose_name = "titre", help_text = "Indiquer le groupe, les unités et le lieu du camp")
    slug = models.SlugField(max_length = 50, verbose_name = "slug", null = True, blank = True)
    intro = models.TextField(verbose_name = "introduction", help_text = "Texte affiché en haut de page, pour présenter brièvement le camp", blank = True, null = True)
    debut = models.DateField(verbose_name = "premier jour du camp", help_text = "Format : jj/mm/aaaa")
    fin = models.DateField(verbose_name = "dernier jour du camp", help_text = "Format : jj/mm/aaaa")
    autoinscription = models.BooleanField(verbose_name = "inscriptions autonomes à la newsletter", help_text = "cocher pour permettre aux familles de s'inscrire de manière autonome à la newsletter", default = True)
    last_newsletter = models.DateTimeField(verbose_name = "date de la dernière newsletter", null = True, blank = True)
    created_by = models.ForeignKey(User, on_delete = models.SET_NULL, null = True, blank = True, related_name = "creator")
    created_time = models.DateTimeField(auto_now_add = True)
    updated_by = models.ForeignKey(User, on_delete = models.SET_NULL, null = True, blank = True, related_name = "updator")
    updated_time = models.DateTimeField(auto_now = True)
    
    class Meta:
        ordering = ['-debut', '-fin']
        models.CheckConstraint(check = models.Q(fin__gt = models.F('debut')), name = "check_debut_fin")
    
    def __str__(self):
        return "{} (du {}/{}/{} au {}/{}/{})".format(self.titre, self.debut.day, self.debut.month, self.debut.year, self.fin.day, self.fin.month, self.fin.year)
    
    def save(self, *args, **kwargs):  # new
        if not self.slug:
            self.slug = slugify(self.titre)
        return super().save(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse("camp", kwargs={"idcamp": self.id, "slug": self.slug})

class Gestion(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE, verbose_name = "utilisateur")
    camp = models.ForeignKey(Camp, on_delete = models.CASCADE, verbose_name = "camp")
    droit = models.CharField(max_length = 20, choices = settings.DROITS, verbose_name = "droit")
    
    class Meta:
        ordering = ['camp', 'droit', 'user']
        models.UniqueConstraint(fields = ['user', 'camp'], name = "gestion_unique_user_camp")
    
    def __str__(self):
        return '{} est {} du camp "{}"'.format(self.user, self.droit, self.camp)

class GestionAjout(models.Model):
    camp = models.ForeignKey(Camp, on_delete = models.CASCADE, verbose_name = "camp")
    mail = models.EmailField(verbose_name = "adresse électonique")
    droit = models.CharField(max_length = 20, choices = settings.DROITS, verbose_name = "droit")
    token = models.CharField(max_length = 64, verbose_name = "token", null = True)
    created_by = models.ForeignKey(User, on_delete = models.CASCADE)
    created_time = models.DateTimeField(auto_now_add = True)
    
    class Meta:
        ordering = ['-created_time']
        models.UniqueConstraint(fields = ['mail', 'camp'], name = "gestionajout_unique_mail_camp")
    
    def __str__(self):
        return 'Ajout d\'un(e) {} au camp "{}"'.format(self.get_droit_display(), self.camp)

class Publication(models.Model):
    camp = models.ForeignKey(Camp, on_delete = models.CASCADE, verbose_name = "Camp")
    created_by = models.ForeignKey(User, on_delete = models.SET_NULL, null = True, related_name = "%(app_label)s_%(class)s_creator")
    created_time = models.DateTimeField(auto_now_add = True)
    updated_by = models.ForeignKey(User, on_delete = models.SET_NULL, null = True, related_name = "%(app_label)s_%(class)s_updator")
    updated_time = models.DateTimeField(auto_now = True)
    
    class Meta:
        abstract = True
        ordering = ['-updated_time']

class Message(Publication):
    texte = models.TextField(verbose_name = "message")
    
    def __str__(self):
        return "Message du {} du camp \"{} \"".format(self.updated_time.strftime("%d/%m/%Y %H:%I"), self.camp)

def chemin_photo_image(instance, fichier):
    return "photos/camp-{}/{}".format(instance.camp.id, fichier)

def chemin_photo_miniature(instance, fichier):
    return "photos/miniatures/camp-{}/{}".format(instance.camp.id, fichier)

class Photo(Publication):
    image = models.ImageField(upload_to = chemin_photo_image, height_field = "image_height", width_field = "image_width", verbose_name = "fichier image")
    image_height = models.IntegerField(verbose_name = "hauteur de l'image")
    image_width = models.IntegerField(verbose_name = "largeur de l'image")
    miniature = models.ImageField(upload_to = chemin_photo_miniature, height_field = "miniature_height", width_field = "miniature_width", verbose_name = "fichier miniature")
    miniature_height = models.IntegerField(verbose_name = "hauteur de la miniature")
    miniature_width = models.IntegerField(verbose_name = "largeur de la miniature")
    
    def __str__(self):
        return "Photo du {} du camp \"{} \"".format(self.updated_time.strftime("%d/%m/%Y %H:%I"), self.camp)

class AbonnementNewsletter(models.Model):
    camp = models.ForeignKey(Camp, on_delete = models.CASCADE, verbose_name = "Camp")
    mail = models.EmailField(verbose_name = "Adresse électronique")
    date = models.DateTimeField(auto_now_add = True)
    statut = models.CharField(max_length = 20, choices = settings.STATUTS_ABO_NEWSLETTER, verbose_name = "statut")
    token = models.CharField(max_length = 64, verbose_name = "token", null = True)
    
    class Meta:
        models.UniqueConstraint(fields = ['camp', 'mail'], name = "abonewsletter_unique_mail_camp")
        ordering = ['-date']
    
    def __str__(self):
        return "Abonnement de {} au camp \"{}\"".format(self.mail, self.camp)
