from django.db.models.signals import pre_delete
from django.dispatch import receiver
from .models import Photo

@receiver(pre_delete, sender = Photo)
def supp_photo(sender, instance, **kwargs):
    instance.image.delete(save = False)
    instance.miniature.delete(save = False)