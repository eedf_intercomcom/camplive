"""
URL configuration for CAMPlive project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from . import views
from . import signals

urlpatterns = [
    path("", views.index, name = "index"),
    path("login", views.login, name = "login"),
    path("logout", views.deconnexion, name = "logout"),
    path("creer-camp", views.paramCamp, name = "creer-camp"),
    path("modifier-camp-<int:idcamp>", views.paramCamp, name = "modifier-camp"),
    path("camp/<int:idcamp>/<slug:slug>", views.camp, name = "camp"),
    path("camp/<int:idcamp>/<slug:slug>/abonnement", views.inscriptionNewsletter, name = "inscription-newsletter"),
    path("camp/<int:idcamp>/<slug:slug>/confirmabonnement-<int:idabo>-<str:token>", views.inscriptionNewsletter, name = "confirmation-newsletter"),
    path("admincamp-<int:idcamp>", views.adminCamp, name = "adminCamp"),
    path("supprcamp-<int:idcamp>", views.supprCamp, name = "supprCamp"),
    path("gestionajout-<int:idcamp>", views.gestionAjout, name = "gestionAjout"),
    path("gestionajoutrappel-<int:idgestionajout>", views.gestionAjoutRappel, name = "gestionAjoutRappel"),
    path("gestionmodif-<int:idgestion>", views.gestionModif, name = "gestionModif"),
    path("gestionsuppr-<int:idgestion>", views.gestionSuppr, name = "gestionSuppr"),
    path("confirmationgestion/<int:idcamp>/<str:token>", views.gestionAjout, name = "confirmation-gestion"),
    path("message-<int:idcamp>", views.message, name = "creer-message"),
    path("modifier-message-<int:idcamp>-<int:idmessage>", views.message, name = "modifier-message"),
    path("supprimer-message-<int:idcamp>-<int:idmessage>", views.supprimerMessage, name = "supprimer-message"),
    path("photo-<int:idcamp>", views.photo, name = "creer-photo"),
    path("download-<int:idcamp>", views.exportZIP, name = "exportZIP"),
    path("supprimer-photo-<int:idcamp>-<int:idphoto>", views.supprimerPhoto, name = "supprimer-photo"),
]