import locale, hashlib
from io import BytesIO
from zipfile import ZipFile
from os.path import basename
from PIL import Image
from itertools import chain
from operator import attrgetter

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import logout
from django.utils import timezone
from django.urls import reverse
from django.core.files import File
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.db.models import Q
from django.http import FileResponse
from django.http.response import Http404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.conf import settings

from .models import *
from .forms import *
from django.urls.base import reverse

# Vérification de droits
def get_camp(request, idcamp, droit = None):
    camp = get_object_or_404(Camp, id = idcamp)
    list_droits = list(settings.DROITS.keys())
    if not droit:
        droit = list_droits[-1]
    if request.user.is_superuser:
        return camp, 'admin'
    else:
        droitgestion = get_object_or_404(Gestion, camp = camp, user = request.user).droit
        if list_droits.index(droitgestion) <= list_droits.index(droit) :
            return camp, droitgestion
        else:
            raise Http404("Vous n'avez pas les droits pour accéder à cette page.")

def get_gestion(request, idgestion):
    gestion = get_object_or_404(Gestion, id = idgestion)
    if request.user.is_superuser or Gestion.objects.filter(droit = "admin", user = request.user, camp = gestion.camp).exclude(user = gestion.user).exists():
        return gestion
    else:
        raise Http404("Vous n'avez pas les droits pour accéder à cette page.")

# Create your views here.
def login(request):
    return render(request, 'camplive/login.html', locals())

def deconnexion(request):
    logout(request)
    messages.success(request,"Vous êtes à présent déconnecté de CAMPlive.")
    return redirect('index')

def index(request):
    """ redirige vers la page d'accueil si non identifié et sur le tableau de bord sinon """
    if request.user.is_authenticated:
        return dashboard(request)
    else:
        return accueil(request)

def accueil(request):
    """ affiche la page d'accueil du site pour les utilisateurs non identifiés """
    return render(request, 'camplive/accueil.html')

@login_required
def dashboard(request):
    """ affiche la liste des camps gérés par l'utilisateur """
    gestion = Gestion.objects.filter(user = request.user)
    idgestion = [g.camp.id for g in gestion]
    camps = list()
    camps_encours = Camp.objects.filter(debut__lte = timezone.now(), fin__gte = timezone.now()).order_by('debut', 'fin')
    camps_futurs = Camp.objects.filter(debut__gt = timezone.now()).order_by('debut', 'fin')
    camps_passes = Camp.objects.filter(fin__lt = timezone.now())
    camps.append({'labels' : "Camp en cours", 'labelp' : "Camps en cours", 'list' : camps_encours})
    camps.append({'labels' : "Camp à venir", "labelp" : "Camps à venir", 'list' : camps_futurs})
    camps.append({'labels' : "Camp terminé", "labelp" : "Camps terminés", 'list' : camps_passes})
    for cat in camps:
        if not request.user.is_superuser:
            cat['list'] = cat['list'].filter(id__in = idgestion)
        for camp in cat['list']:
            camp.gestion = camp.gestion_set.filter(user = request.user)
    return render(request, 'camplive/dashboard.html', {"camps": camps})

@login_required
def paramCamp(request, idcamp = None):
    """ créer ou modifier un nouveau camp """
    if idcamp:
        camp, droitgestion = get_camp(request, idcamp, 'admin')
    else:
        camp = None
    if request.method == "POST":
        form = CampForm(request.POST, instance = camp)
        if form.is_valid():
            camp = form.save()
            if not idcamp:
                Gestion.objects.create(user = request.user, camp = camp, droit = 'admin')
                camp.created_by = request.user
                messages.success(request, "Le camp est créé.")
            else:
                messages.success(request, "Le camp a été mis à jour.")
                camp.updated_by = request.user
            camp.save()
            return redirect('adminCamp', camp.id)
    else:
        form = CampForm(instance = camp)
    return render(request, 'camplive/creer_camp.html', {'form': form, 'modif': idcamp})

@login_required
def adminCamp(request, idcamp):
    """ permet d'administrer ou contribuer à un camp """
    camp, droitgestion = get_camp(request, idcamp)
    gestions = Gestion.objects.filter(camp = camp)
    gestionAjouts = GestionAjout.objects.filter(camp = camp)
    publi_messages = Message.objects.filter(camp = camp).order_by('-created_time')
    message_form = MessageForm(request.POST or None)
    publi_photos = Photo.objects.filter(camp = camp)
    photo_form = PhotoForm(request.POST or None)
    abonnes = AbonnementNewsletter.objects.filter(camp = camp).order_by('-statut', 'mail')
    return render(request, 'camplive/admin_camp.html', locals())

@login_required
def supprCamp(request, idcamp):
    """ supprime un camp """
    camp, droitgestion = get_camp(request, idcamp, 'admin')
    camp.delete()
    messages.success(request, "Le camp (messages, photos, abonné(e)s) a été supprimé.")
    return redirect('index')

@login_required
def gestionAjout(request, idcamp, token = None):
    """ permet d'ajouter un gestionnaire """
    if token:
        camp = get_object_or_404(Camp, id = idcamp)
        gestionAjout = get_object_or_404(GestionAjout, camp = camp, token = token)
        if Gestion.objects.filter(camp = camp, user = request.user).exists():
            messages.error(request, "Vous avez déjà des droits attribués sur la gestion de camp. Veuillez contacter un administrateur.")
        else:
            gestion = Gestion.objects.create(
                camp = camp,
                droit = gestionAjout.droit,
                user = request.user
            )
            gestionAjout.delete()
            messages.success(request, "Vous êtes désormais {} de la page CAMPlive de \"{}\".".format(gestion.get_droit_display(), camp))
        return redirect('adminCamp', idcamp = camp.id)
    else:
        camp, gestiondroit = get_camp(request, idcamp, "admin")
        if request.method == "POST":
            form = GestionAjoutForm(request.POST)
            if form.is_valid():
                gestionAjout = form.save(commit = False)
                gestionAjout.camp = camp
                if GestionAjout.objects.filter(camp = camp, mail = gestionAjout.mail).exists():
                    messages.error(request, "Une invitation est déjà lancée pour cette adresse mail.")
                else:
                    gestionAjout.created_by = request.user
                    gestionAjout.token = hashlib.sha256(bytes("{}{}".format(camp, timezone.datetime.now()), 'utf-8')).hexdigest()
                    gestionAjout.save()
                    mail_html_content = "{} vous invite à gérer la page CAMPlive du camp \"{}\" avec le rôle {}.\nPour confirmer votre inscription, veuillez cliquer sur le lien suivant : {}://{}{}".format(request.user, camp, gestionAjout.get_droit_display(), request.scheme, request.get_host(), reverse('confirmation-gestion', kwargs = {'idcamp': camp.id, 'token': gestionAjout.token}))
                    mail_text_content = """{} vous invite à gérer la page CAMPlive du camp \"{}\" avec le rôle {}.
        Pour confirmer votre inscription, veuillez cliquer sur le lien suivant : {}://{}{}""".format(request.user, camp, gestionAjout.get_droit_display(), request.scheme, request.get_host(), reverse('confirmation-gestion', kwargs = {'idcamp': camp.id, 'token': gestionAjout.token}))
                    send_mail(
                        subject = "Invitation à gérer une page CAMPlive",
                        message = render_to_string('camplive/mail_text.txt', {'mail_text_content': mail_text_content, 'host': get_current_site(request).domain}),
                        html_message = render_to_string('camplive/mail_html.html', {'mail_html_content': mail_html_content, 'host': get_current_site(request).domain}),
                        from_email = "CAMP live<camplive@galilee.eedf.fr>",
                        recipient_list = [gestionAjout.mail],
                        fail_silently = False
                    )
                    messages.success(request, "L'invitation a été envoyée.")
                    return redirect('adminCamp', idcamp = camp.id)
        else:
            form = GestionAjoutForm()
    return render(request, 'camplive/gestion_ajout.html', locals())

@login_required
def gestionAjoutRappel(request, idgestionajout):
    gestionAjout = get_object_or_404(GestionAjout, id = idgestionajout)
    camp, droit = get_camp(request, gestionAjout.camp.id, droit = 'admin')
    mail_html_content = "{} vous invite à gérer la page CAMPlive du camp \"{}\" avec le rôle {}.\nPour confirmer votre inscription, veuillez cliquer sur le lien suivant : {}://{}{}".format(request.user, camp, gestionAjout.get_droit_display(), request.scheme, request.get_host(), reverse('confirmation-gestion', kwargs = {'idcamp': camp.id, 'token': gestionAjout.token}))
    mail_text_content = """{} vous invite à gérer la page CAMPlive du camp \"{}\" avec le rôle {}.
Pour confirmer votre inscription, veuillez cliquer sur le lien suivant : {}://{}{}""".format(request.user, camp, gestionAjout.get_droit_display(), request.scheme, request.get_host(), reverse('confirmation-gestion', kwargs = {'idcamp': camp.id, 'token': gestionAjout.token}))
    send_mail(
        subject = "Rappel : Invitation à gérer une page CAMPlive",
        message = render_to_string('camplive/mail_text.txt', {'mail_text_content': mail_text_content, 'host': get_current_site(request).domain}),
        html_message = render_to_string('camplive/mail_html.html', {'mail_html_content': mail_html_content, 'host': get_current_site(request).domain}),
        from_email = "CAMP live<camplive@galilee.eedf.fr>",
        recipient_list = [gestionAjout.mail],
        fail_silently = False
    )
    messages.success(request, "Un mail a été renvoyé à {}. Conseillez-lui de consulter son courrier indésirable si nécessaire.".format(gestionAjout.mail))
    return redirect('adminCamp', idcamp = camp.id)
    
@login_required
def gestionModif(request, idgestion):
    gestion = get_gestion(request, idgestion)
    if request.method == "POST":
        form = GestionModifForm(request.POST, instance = gestion)
        if form.is_valid():
            form.save()
            messages.success(request, "Les modifications sont sauvegardées.")
            return redirect('adminCamp', idcamp = gestion.camp.id)
    else:
        form = GestionModifForm(instance = gestion)
    return render(request, 'camplive/gestion_modif.html', locals())

@login_required
def gestionSuppr(request, idgestion):
    gestion = get_gestion(request, idgestion)
    gestion.delete()
    messages.success(request, "{} a été supprimé(e) des gestionnaires.".format(gestion.user))
    return redirect('adminCamp', idcamp = gestion.camp.id)

def afficherdate(date1, date2):
    """ rédige la période entre deux dates : du ... au ... """
    locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
    if date1==date2:
        return date1.strftime('%A %d %B %Y')
    else:
        retour=date1.strftime('du %A %d ')
        if date1.year!=date2.year or date1.month!=date2.month: retour+=date1.strftime('%B ')
        if date1.year!=date2.year: retour+=date1.strftime('%Y ')
        retour+=' au '+date2.strftime('%A %d %B %Y')
        return retour

def camp(request, idcamp, slug):
    """ affiche messages et photos d'un camp """
    camp = get_object_or_404(Camp, id = idcamp, slug = slug)
    gestionnaire = request.user.is_authenticated and (Gestion.objects.filter(camp = camp, user = request.user).exists() or request.user.is_superuser)
    dates = afficherdate(camp.debut, camp.fin)
    newsform = AbonnementNewsletterForm()
    publi_messages = Message.objects.filter(camp = camp)
    publi_photos = Photo.objects.filter(camp = camp)
    publis = sorted(
        chain(publi_messages, publi_photos),
        key = attrgetter('created_time'),
        reverse = True
    )
    publications = dict()
    for publi in publis:
        date = publi.created_time.date()
        if date not in publications.keys(): publications[date] = list()
        if type(publi) == Message: publications[date].append(('message', publi))
        elif type(publi) == Photo:
            if len(publications[date]) == 0 or publications[date][-1][0] != 'photo':
                publications[date].append(('photo', list()))
            publications[date][-1][1].append(publi)
    return render(request, 'camplive/camp.html', locals())

@login_required
def message(request, idcamp, idmessage = None):
    """ permet de créer ou modifier un message """
    camp, droitgestion = get_camp(request, idcamp)
    if idmessage: message = Message.objects.get(id = idmessage)
    else: message = None
    if request.method == "POST":
        form = MessageForm(request.POST, instance = message)
        if form.is_valid():
            message = form.save(commit = False)
            message.camp = camp
            if idmessage:
                message.updated_by = request.user
                messages.success(request, "Le message a été modifié.")
            else:
                message.created_by = request.user
                messages.success(request, "Le message a été posté.")
            message.save()
            return redirect('adminCamp', idcamp = camp.id)
    else:
        form = MessageForm(instance = message)
    return render(request, 'camplive/message.html', {'form': form, 'modif': idmessage})

@login_required
def supprimerMessage(request, idcamp, idmessage):
    """ supprime un message """
    camp, droitgestion = get_camp(request, idcamp)
    message = get_object_or_404(Message, id = idmessage)
    message.delete()
    messages.success(request, "Le message a été supprimé.")
    return redirect('adminCamp', idcamp = camp.id)

@login_required
def photo(request, idcamp):
    """ permet d'ajouter une photo """
    camp, droitgestion = get_camp(request, idcamp)
    if request.method == "POST":
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            files = request.FILES.getlist('image')
            for fichier in files:
                im = Image.open(fichier)
                exif = im._getexif()
                # if image has exif data about orientation, let's rotate it
                orientation_key = 274 # cf ExifTags
                if exif and orientation_key in exif:
                    orientation = exif[orientation_key]
                    rotate_values = {
                        3: Image.ROTATE_180,
                        6: Image.ROTATE_270,
                        8: Image.ROTATE_90
                    }
                    if orientation in rotate_values:
                        im = im.transpose(rotate_values[orientation])
                
                h = im.height
                w = im.width
                reduc = im.resize((int(1000/h*w), 1000), Image.LANCZOS)
                thumb_io = BytesIO()
                reduc.save(thumb_io, im.format, quality=85)
                reducfile = File(thumb_io, name=fichier.name)
                miniature = im.resize((int(300/h*w),300))
                thumb_io = BytesIO() # create a BytesIO object
                miniature.save(thumb_io, im.format, quality=85) # save image to BytesIO object
                thumbnail = File(thumb_io, name=fichier.name) # create a django friendly File object
                photo = Photo()
                photo.camp = camp
                photo.image = reducfile
                photo.miniature = thumbnail
                photo.created_by = request.user
                photo.save()
            if len(files) == 1:
                messages.success(request, "La photo a été publiée.")
            else:
                messages.success(request, "{} photos ont été publiées.".format(len(files)))
    return redirect('adminCamp', idcamp = camp.id)

@login_required
def supprimerPhoto(request, idcamp, idphoto):
    """ supprime une photo """
    camp, droitgestion = get_camp(request, idcamp)
    photo = get_object_or_404(Photo, id = idphoto)
    photo.delete()
    messages.success(request, "La photo a été supprimée.")
    return redirect('adminCamp', idcamp = camp.id)

@login_required
def exportZIP(request, idcamp):
    """ créer une archive ZIP des photos du camp """
    camp, droitgestion = get_camp(request, idcamp)
    buffer = BytesIO()
    zip = ZipFile(buffer, "w")
    for photo in Photo.objects.filter(camp = camp):
        zip.write(photo.image.path, arcname = '/photos/' + basename(photo.image.name))
    messagestxt = "Message postés sur le CAMPlive du camp \"{}\"".format(camp)
    for message in Message.objects.filter(camp = camp).order_by('created_time'):
        messagestxt += "\n\nMessage du {} :\n{}".format(
            message.created_time.strftime("%d/%m/%Y à %H:%I"),
            message.texte
        )
    zip.writestr("messages.txt", messagestxt)
    zip.close()
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename="{}.zip".format(camp.slug))

def inscriptionNewsletter(request, idcamp, slug, idabo = None, token = None):
    """ gère les inscriptions/confirmations à une newsletter """
    camp = get_object_or_404(Camp, id = idcamp, slug = slug, autoinscription = True)
    if idabo and token:
        aboNewsletter = get_object_or_404(AbonnementNewsletter, id = idabo, token = token, camp = camp, statut = "confirm")
        aboNewsletter.token = None
        aboNewsletter.statut = "inscrit"
        aboNewsletter.save()
        messages.success(request, "Votre inscription à la newsletter est validée.")
    else:
        form = AbonnementNewsletterForm(request.POST)
        if form.is_valid():
            aboNewsletter = form.save(commit = False)
            query = AbonnementNewsletter.objects.filter(camp = camp, mail = aboNewsletter.mail)
            if query.filter(statut = "inscrit").exists():
                messages.error(request, "Un abonnement est déjà pris en compte pour cette adresse mail.")
            else:
                confirm = query.filter(statut = "confirm").exists()
                if not confirm:
                    aboNewsletter.camp = camp
                    aboNewsletter.statut = "confirm"
                    aboNewsletter.token = hashlib.sha256(bytes("{}{}{}randomhash".format(aboNewsletter.mail, slug, timezone.datetime.now()), 'utf-8')).hexdigest()
                    aboNewsletter.save()
                else:
                    aboNewsletter = query.get(statut = "confirm")
                mail_html_content = "Vous avez demandé à recevoir la newsletter du camp \"{}\" à cette adresse.\nPour confirmer votre inscription, veuillez cliquer sur le lien suivant : {}://{}{}".format(camp, request.scheme, request.get_host(), reverse('confirmation-newsletter', kwargs = {'idcamp': camp.id, 'slug': camp.slug, 'idabo': aboNewsletter.id, 'token': aboNewsletter.token}))
                mail_text_content = """Vous avez demandé à recevoir la newsletter du camp \"{}\" à cette adresse.
Pour confirmer votre inscription, veuillez suivre le lien suivant : {}://{}{}""".format(camp, request.scheme, request.get_host(), reverse('confirmation-newsletter', kwargs = {'idcamp': camp.id, 'slug': camp.slug, 'idabo': aboNewsletter.id, 'token': aboNewsletter.token}))
                send_mail(
                    subject = "Inscription à une newsletter",
                    message = render_to_string('camplive/mail_text.txt', {'mail_text_content': mail_text_content, 'host': get_current_site(request).domain}),
                    html_message = render_to_string('camplive/mail_html.html', {'mail_html_content': mail_html_content, 'host': get_current_site(request).domain}),
                    from_email = "CAMP live<camplive@galilee.eedf.fr>",
                    recipient_list = [aboNewsletter.mail],
                    fail_silently = False
                )
                if confirm:
                    messages.warning(request, "Une demande d'abonnement est en attente de confirmation pour cette adresse mail ; veuillez vérifier votre messagerie (vérifiez votre courrier indésirable si nécessaire).")
                else:
                    messages.success(request, "Votre demande d'abonnement est prise en compte. Veuillez consulter votre messagerie pour confirmer votre inscription à la newsletter (vérifiez votre courrier indésirable si nécessaire).")
    return redirect('camp', idcamp = camp.id, slug = slug)

