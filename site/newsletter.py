"""
À exécuter régulièrement (tâche cron)
"""

import django, sys, os
import CAMPlive
from django.template.defaulttags import now
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "CAMPlive.settings")
django.setup()

from django.conf import settings
from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.utils import timezone
from django.urls import reverse
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from camplive.models import Camp, Message, Photo
import locale

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
current_tz = timezone.get_current_timezone()

now = timezone.now()

for camp in Camp.objects.all():
    if camp.message_set.filter(created_time__gte = now - timezone.timedelta(minutes = settings.DELAI_NEWSLETTER)).count() + camp.photo_set.filter(created_time__gte = now - timezone.timedelta(minutes = settings.DELAI_NEWSLETTER)).count() == 0:
        messages = camp.message_set.all()
        photos = camp.photo_set.all()
        if camp.last_newsletter:
            messages = messages.filter(created_time__gte = camp.last_newsletter)
            photos = photos.filter(created_time__gte = camp.last_newsletter)
        if messages.count() + photos.count() > 0:
            notif = ""
            if messages.count() > 0:
                if messages.count() == 1:
                    notif = "1 nouveau message"
                else:
                    notif = "{} nouveaux messages".format(messages.count())
            if photos.count() > 0:
                if len(notif) > 0:
                    notif += " et "
                if photos.count() == 1:
                    notif += "1 nouvelle photo"
                else:
                    notif += "{} nouvelles photos".format(photos.count())
            if messages.count() + photos.count() == 1:
                sujet = "Nouvelle publication CAMPlive"
                notif += " est disponible"
            else:
                sujet = "Nouvelles publications CAMPlive"
                notif += " sont disponibles"
            notif += " sur la page CAMPlive du camp \"{}\" : https://{}{}".format(camp, CAMPlive.settings.ALLOWED_HOSTS[0], reverse('camp', kwargs = {'idcamp': camp.id, 'slug': camp.slug}))
            mails = list()
            for abo in camp.abonnementnewsletter_set.filter(statut = "inscrit"):
                email = EmailMultiAlternatives(
                    sujet,
                    render_to_string('camplive/mail_text.txt', {'mail_text_content': notif, 'host': Site.objects.get_current().domain}),
                    "CAMPlive<camplive@galilee.eedf.fr>",
                    [abo.mail]
                )
                email.attach_alternative(render_to_string('camplive/mail_html.html', {'mail_html_content': notif, 'host': Site.objects.get_current().domain}), 'text/html')
                mails.append(email)
            # Envoi de la newsletter
            connection = mail.get_connection()
            connection.open()
            connection.send_messages(mails)
            connection.close()
            camp.last_newsletter = now
            camp.save()
